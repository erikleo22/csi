import os
from datetime import datetime
import pytz

class Config:
    ##################### folder paths #####################
    csv_folder = os.path.abspath("../CSI_Data")
    label_folder = os.path.abspath("../CSI_Labels")
    temp_humidity_file = os.path.abspath("../temperature_and_humidity.csv")
    cache_folder = os.path.abspath("../CSI_Cache")
    results_folder = os.path.abspath("../Results")


    ##################### CSI subcarriers #####################
    num_subcarriers = 128
    null_subcarriers = [0,1,2,3,4,5,63,64,65,123,124,125,126,127]
    pilot_subcarriers = [11,39,53,75,89,117]
    manual_co_subcarriers = list(range(64,127))
    cross_out_subcarriers = list(set(null_subcarriers + pilot_subcarriers + manual_co_subcarriers))

    ##################### neural network #####################
    network_type = "MLP" # "SVM" or "MLP"
    split_mode = "days" # "days" or "persons" or "manual"
    splits = 5

    validation_days = {
        "with person": ["March_6", "March_11", "March_18"],
        "without person": ["March_6", "March_11", "March_18"]
    }
    training_days = {
        "with person": ["January_19", "January_31", "February_6", "February_12", "January_11", "January_16"],
        "without person": ["January_16", "January_19", "January_31", "February_6", "February_12", "January_11", "February_8"]
    }

    validation_persons = {
        "with person": ["Leo", "Sophie", "Daniel", "Mingqing"],
        "without person": ["nobody"]
    }
    training_persons = {
        "with person": ["Alejandro", "Ian", "Saeed", "Ana", "Florian", "Hussein"],
        "without person": ["nobody"]
    }

    training_manual = {
        "with person": ["CSI_March_18_24_15_17_18"],
        "without person": ["CSI_March_18_24_15_23_38"]
    }
    validation_manual = {
        "with person": ["CSI_March_18_24_15_30_28"],
        "without person": ["CSI_March_18_24_15_33_27"]
    }

    training_data_percentage_min = 0.7
    training_data_percentage_max = 0.95

    ##################### neural network measures #####################
    network_measures = {
        "SVM":{
            "C": 10,
            "kernel": "poly",
            "degree": 3,
            "gamma": 0.1,
            "verbose": True
        },
        "MLP":{
            "hidden_layer_sizes": (1000,500),
            "max_iter": 16,
            "activation": "relu",
            "solver": "adam",
            "warm_start": False, # should be kept for holding out of data
            "early_stopping": True,
            "verbose": True,
            "batch_size": 32
        }
    }

    ##################### preprocessing #####################
    # flags:
    outlier_removal: bool = False
    discrete_wavelet_transformation: bool = False
    normalization: bool = False
    use_packet_windows: bool = True

    logging_dir_name = datetime.now(tz=pytz.timezone('Europe/Berlin')).strftime("%Y_%m_%d-%H_%M_%S") + f"_{network_type}_" + split_mode
    os.mkdir(os.path.join(results_folder, logging_dir_name))

    outlier_removal_window = 5
    outlier_removal_threshold = 1 #factor of std deviation, values have to be inside

    packet_window = 10 # how many udp packets should be considered as one data point

    ############################## logging ###############################
    max_num_packets_logging = 100
    
    
    ########################### unwanted data: ###########################
    # keys: name of dataset, where certain packets should be removed
    # values: tuples with amount of seconds from which slicing should start
    #         and amount of seconds, that should be removed at the end of
    #         the data set

    unwanted_data = {
        "CSI_January_11_24_11_18_09": (0, 5),
        "CSI_March_18_24_15_30_28": (5, 5),
        "empty": (5, 5) # for data sets without a person
    }
    