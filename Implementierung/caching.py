from config import Config
from csv_processor import CSV
from sensor_processor import Sensor
from preprocessor import Preprocessor
import os
import json
import itertools

class Cache:
    def __init__(self):
        self.con = Config()
        self.csv = CSV()
        self.sensor = Sensor()
        self.preprocessor = Preprocessor()


    def receive_csi_data_sets(self):
        csv_file_list = self.csv.get_csv_file_list()
        csv_files_not_cached = self.get_not_cached_files_list(csv_file_list)
        self.csv.read_csi_data(csv_files_not_cached)
        self.write_to_cache_files(csv_files_not_cached)
        return self.read_from_cache_files(os.listdir(self.con.cache_folder))


    def get_not_cached_files_list(self, csv_file_list):
        csv_files_not_cached = []
        for csv_file_name in csv_file_list:
            cache_file_base = os.path.join(self.con.cache_folder, csv_file_name.split(".")[0])
            if self.con.discrete_wavelet_transformation & self.con.outlier_removal:
                cache_file_base += "_dwt_rmo"
            elif self.con.discrete_wavelet_transformation:
                cache_file_base += "_dwt"
            elif self.con.outlier_removal:
                cache_file_base += "_rmo"
            if not os.path.isfile(cache_file_base + ".txt"):
                csv_files_not_cached.append(csv_file_name)
            
        return csv_files_not_cached

    
    def write_to_cache_files(self, csv_files_not_cached):
        for csv_file_name in csv_files_not_cached:
            csi_data_set = self.csv.CSI_data_sets[csv_file_name.split(".")[0]]
            if self.con.outlier_removal:
                csi_data_set = self.preprocessor.remove_outliers_data_sets(csi_data_set)
            if self.con.discrete_wavelet_transformation:
                csi_data_set = self.preprocessor.perform_dwt_data_sets(csi_data_set)
            with open(self.get_cache_file_path(csv_file_name), 'x') as file:
                file.write(json.dumps(csi_data_set, indent=3))
        return
    

    def write_sensor_data_to_cache(self, data_set_name, csi_data_set):
        with open(self.get_cache_file_path(data_set_name), 'w') as file:
            file.write('')
            file.write(json.dumps(csi_data_set, indent=3))
    

    def read_from_cache_files(self, files_list):
        csi_data_sets = dict()
        for cache_file in files_list:
            with open(self.get_cache_file_path(cache_file), 'r') as file:
                data = file.read()
                data_set_name = cache_file.split(".")[0].strip("_rmo").strip("_dwt")
                csi_data_sets[data_set_name] = json.loads(data)
        return csi_data_sets
    

    def get_csi_and_sensor_data(self, data_set_name, sensor_data_set, csi_data_set, person_position):
        cached_csi_data_set = self.read_from_cache_files([data_set_name])
        for csi_time_stamp, csi_data_point in cached_csi_data_set[data_set_name]["data"].items():
            if "temperature" in list(csi_data_point.keys()): #sensor data in cache file
                return self.remove_unwanted_data(data_set_name, cached_csi_data_set, person_position)
            else:
                cached_csi_data_set = self.sensor.match_sensor_data(sensor_data_set, csi_data_set)
                self.write_sensor_data_to_cache(data_set_name, cached_csi_data_set)
                return self.remove_unwanted_data(data_set_name, cached_csi_data_set, person_position)                    


    def remove_unwanted_data(self, data_set_name, csi_data_set, person_position):
        if data_set_name in csi_data_set.keys():
            csi_data_set = csi_data_set[data_set_name]

        if (person_position == "") & ("empty" in self.con.unwanted_data.keys()):
            start = self.con.unwanted_data["empty"][0] * 10
            stop = len(csi_data_set["data"]) - self.con.unwanted_data["empty"][1] * 10
            csi_data_set["data"] = dict(itertools.islice(csi_data_set["data"].items(), start, stop))
            
        elif data_set_name in self.con.unwanted_data:
            start = self.con.unwanted_data[data_set_name][0] * 10
            stop = len(csi_data_set["data"]) - self.con.unwanted_data[data_set_name][1] * 10
            csi_data_set["data"] = dict(itertools.islice(csi_data_set["data"].items(), start, stop))
        
        return csi_data_set


    def get_cache_file_path(self, file_name):
        cache_file_base = file_name.split(".")[0]
        if "_dwt" in cache_file_base or "_rmo" in cache_file_base:
            return os.path.join(self.con.cache_folder, cache_file_base + ".txt")
        
        if self.con.discrete_wavelet_transformation & self.con.outlier_removal:
            cache_file_base += "_dwt_rmo"
        elif self.con.discrete_wavelet_transformation:
            cache_file_base += "_dwt"
        elif self.con.outlier_removal:
            cache_file_base += "_rmo"
        return os.path.join(self.con.cache_folder, cache_file_base + ".txt")
