from config import Config
import os, json


class LabelProcessor:
    def __init__(self):
        self.con = Config()
        self.label_data_sets = dict()

    def read_label_files(self):
        label_files = self.get_label_files()
        for label_file in label_files:
            label_data = self.receive_label_data(label_file)
            if not label_data == {}: self.label_data_sets[label_file.split('.')[0]] = label_data
        return self.label_data_sets

    def get_label_files(self):
        files_list = []
        [files_list.append(file) for file in os.listdir(self.con.label_folder) if '.txt' in file[-4:]]
        return files_list

    def receive_label_data(self, label_file):
        with open(os.path.join(self.con.label_folder, label_file)) as file:
            label_data = file.read()
            try:
                label_dict_raw = json.loads(label_data)
            except:
                label_dict_raw = {}

        return label_dict_raw
