from config import Config
from preprocessor import Preprocessor
from logger import Logger
from sklearn import svm, neural_network
from sklearn.preprocessing import *
from datetime import datetime
import pytz
import matplotlib.pyplot as plt


class Trainer():
    def __init__(self):
        self.con = Config()
        self.preprocessor = Preprocessor()
        self.logger = Logger()


    def train_model(self, data_sets):
        #self.preprocessor.perform_dwt_data_sets(data_sets)
        #(data, labels) = self.preprocessor.rearrange_data(data_sets)
        data_splits = self.preprocessor.get_data_split(data_sets, splits=self.con.splits)
        if self.con.network_type == 'SVM':
            self.train_svm(data_sets, data_splits, self.con.logging_dir_name)
        elif self.con.network_type == 'MLP':
            self.train_mlp(data_sets, data_splits, self.con.logging_dir_name)


    def train_mlp(self, data_sets, data_splits, dir_name):
        clf = neural_network.MLPClassifier(
            hidden_layer_sizes=self.con.network_measures["MLP"]["hidden_layer_sizes"], 
            max_iter=self.con.network_measures["MLP"]["max_iter"],
            activation =self.con.network_measures["MLP"]["activation"],
            solver=self.con.network_measures["MLP"]["solver"],
            warm_start=self.con.network_measures["MLP"]["warm_start"],
            early_stopping=self.con.network_measures["MLP"]["early_stopping"],
            verbose=self.con.network_measures["MLP"]["verbose"],
            batch_size=self.con.network_measures["MLP"]["batch_size"])
        
        self.train_network(clf, data_splits, data_sets, dir_name)
        return
        

    def train_svm(self, data_sets, data_splits, dir_name):
        clf = svm.SVC(C=self.con.network_measures["SVM"]["C"],
                      kernel=self.con.network_measures["SVM"]["kernel"], 
                      degree=self.con.network_measures["SVM"]["degree"], 
                      gamma=self.con.network_measures["SVM"]["gamma"],
                      verbose=self.con.network_measures["SVM"]["verbose"])
        
        self.train_network(clf, data_splits, data_sets, dir_name)
        return


    def train_network(self, clf, data_splits, data_sets, dir_name):
        fold=0
        self.logger.save_data_splits(dir_name, data_splits, data_sets)
        plt.clf()
        for data_split in data_splits:
            #self.logger.print_data_set_measures(data_split["training"]["y"], data_split["validation"]["y"], labels)
            clf.fit(data_split["training"]["X"], data_split["training"]["y"])
            y_pred = clf.predict(data_split["validation"]["X"])
            self.logger.print_result(clf, data_split["validation"]["y"], y_pred, fold)
            self.logger.save_result(clf, dir_name, data_split, y_pred, fold)
            self.logger.save_loss_curve(clf, dir_name, fold)
            fold+=1
        
        return
