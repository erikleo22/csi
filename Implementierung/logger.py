from config import Config
import numpy as np
from sklearn import metrics
import os
import matplotlib.pyplot as plt
import csv
import pandas as pd

class Logger:
    def __init__(self):
        self.con = Config()


    def print_average_data_of_person_types(self, dir_name, X, y, file_base_name):
        X_person, X_nobody = np.array([]), np.array([])
        nobodies, persons = 0,0
        filepath_person = os.path.join(self.con.results_folder, dir_name, file_base_name + "_person.png")
        filepath_nobody = os.path.join(self.con.results_folder, dir_name, file_base_name + "_nobody.png")
        fig_person, ax_person = plt.subplots()
        fig_nobody, ax_nobody = plt.subplots()

        for index, x in enumerate(X):
            #if (nobodies + persons) >= 4000:
            #    break
            if (y[index] == 1):# & (persons < 2000):
                X_person = x if X_person.shape[0] == 0 else np.vstack([X_person, x])
                #persons += 1
                #ax_person.plot(x)
                #fig_person.savefig(filepath_person)
            elif (y[index] == 0):# & (nobodies < 2000):
                X_nobody = x if X_nobody.shape[0] == 0 else np.vstack([X_nobody, x])
                #nobodies += 1
                #ax_nobody.plot(x)
                #fig_nobody.savefig(filepath_nobody)
                    
        mean_person = X_person.mean(axis=0)
        std_person = np.std(X_person, axis=0)
        mean_nobody = X_nobody.mean(axis=0)
        std_nobody = np.std(X_nobody, axis=0)
        ax_person.plot(mean_person)
        ax_person.fill_between(range(mean_person.shape[0]), mean_person+std_person, mean_person-std_person, facecolor="orange", alpha=0.5)
        fig_person.savefig(filepath_person)
        ax_nobody.plot(mean_nobody)
        ax_nobody.fill_between(range(mean_nobody.shape[0]), mean_nobody+std_nobody, mean_nobody-std_nobody, facecolor="orange", alpha=0.5)
        fig_nobody.savefig(filepath_nobody)

    
    def print_data_set_measures(self, y_train, y_test, labels):
        labels_sum = y_train.shape[0] + y_test.shape[0]
        print("Train/Test-Ratio: " + str(y_train.shape[0]/labels_sum * 100) + "/" 
              + str(y_test.shape[0]/labels_sum * 100))
        
        print("Ones in data: " + str(np.count_nonzero(labels) / labels.shape[0] * 100))
        print("Ones ratio in train set: " + str(np.count_nonzero(y_train)/y_train.shape[0] * 100))
        print("Ones ratio in test set: " + str(np.count_nonzero(y_test)/y_test.shape[0] * 100))
        return


    def save_result(self, clf, dir_name, data_split, y_pred, fold):
        filepath = os.path.join(self.con.results_folder, dir_name, "result.txt")
        if os.path.exists(filepath):
            with open(filepath, 'a') as file:
                self.write_fold_measures(clf, file, fold, data_split, y_pred)
            return

        else:
            with open(filepath, 'x') as file:
                self.write_network_measures(file)
                self.write_fold_measures(clf, file, fold, data_split, y_pred)
            return


    def save_data_splits(self, dir_name, data_splits, data_sets):
        filepath = os.path.join(self.con.results_folder, dir_name, "data_sets.txt")
        self.save_data_split_csv(filepath.strip(".txt") + ".csv", data_splits, data_sets)
        with open(filepath, 'x') as file:
            self.write_data_split_measures(file, data_splits, data_sets)


    def save_data_split_csv(self, filepath, data_splits, data_sets):
        #persons = []
        #[persons.append(person for person in data_split["training"]["persons"]) for data_split in data_splits]
        #persons = list(set(persons))
        header = [""]
        header.append(f"Fold{fold}" for fold in range(1, self.con.splits + 1))
        person_and_day_ratios = self.get_person_and_day_ratios(data_splits, data_sets)
        
        data = {
            "person_ratios": {"training": [], "validation": []},
            "day_ratios": {"training": [], "validation": []}
        }
        data_measures = {
            "training": [], 
            "validation": []
        }
        table_head = list(person_and_day_ratios.keys())
        table_head.insert(0, 'fold')
        [data['person_ratios'][data_set_type].append(table_head) for data_set_type in ["training", "validation"]]
        [data['day_ratios'][data_set_type].append(table_head) for data_set_type in ["training", "validation"]]
        
        persons = {"training": [], "validation": []}
        days = {"training": [], "validation": []}

        for fold_index, fold in person_and_day_ratios.items():
            for data_set_type in ["training", "validation"]:
                for person in list(fold[data_set_type]["person_ratios"].keys()):
                    persons[data_set_type].append(person)
                for day in list(fold[data_set_type]["day_ratios"].keys()):
                    days[data_set_type].append(day)

        for data_set_type in ["training", "validation"]:
            persons[data_set_type] = list(set(persons[data_set_type]))
            days[data_set_type] = list(set(days[data_set_type]))

        [persons[data_set_type].sort() for data_set_type in ["training", "validation"]]
        [data["person_ratios"][data_set_type].append([person]) for data_set_type in ["training", "validation"] for person in persons[data_set_type]]
        [data_measures[data_set_type].append(["packets"]) for data_set_type in ["training", "validation"]]
        [days[data_set_type].sort() for data_set_type in ["training", "validation"]]
        [data["day_ratios"][data_set_type].append([day]) for data_set_type in ["training", "validation"] for day in days[data_set_type]]

        for fold_index, fold in person_and_day_ratios.items():
            for data_set_type in ["training", "validation"]:
                num_packets = data_splits[fold_index][data_set_type]["packets"]
                data_measures[data_set_type][0].append(num_packets)
                for person in persons[data_set_type]:
                    person_ratio = fold[data_set_type]["person_ratios"].get(person)
                    if person_ratio == None: format_person_ratio = '-'
                    else: format_person_ratio = "{:.1%}".format(person_ratio)
                    data["person_ratios"][data_set_type][persons[data_set_type].index(person) + 1].append(format_person_ratio)
                for day in days[data_set_type]:
                    day_ratio = fold[data_set_type]["day_ratios"].get(day)
                    if day_ratio == None: format_day_ratio = '-'
                    else: format_day_ratio = "{:.1%}".format(day_ratio)
                    data["day_ratios"][data_set_type][days[data_set_type].index(day) + 1].append(format_day_ratio)


        with open(filepath, 'x') as file:
            csv_writer = csv.writer(file, dialect='excel')
            headline = [""] * len(table_head)
            csv_writer.writerow(headline)
            for data_type, packets in data_measures.items():
                headline = [""] * len(packets[0])
                headline[0] = f"basic measures - {data_set_type}"
                csv_writer.writerow(headline)
                csv_writer.writerows(packets)
                csv_writer.writerow([''])

            for days_or_persons, days_or_persons_data in data.items():
                for data_type, ratios in days_or_persons_data.items():
                    headline = [""] * len(ratios[0])
                    headline[0] = f"{data_type} - {days_or_persons}"
                    csv_writer.writerow(headline)
                    csv_writer.writerows(ratios)
                    csv_writer.writerow([''])

            file.close()


        writer = pd.ExcelWriter(filepath.strip("csv") + "xlsx", engine='xlsxwriter')
        csv_read_file = pd.read_csv(filepath)
       
        csv_read_file.to_excel(writer, 
                               sheet_name="Daten-Evaluation",
                               index=None,
                               header=False)
        writer.sheets["Daten-Evaluation"].set_column(0,0,11)
        writer.close()

        return


    def write_network_measures(self, file):
        file.write(f"Pre-processing:\n\n")
        file.write(f"Outlier Removal: {self.con.outlier_removal}\n")
        file.write(f"Discrete Wavelet: {self.con.discrete_wavelet_transformation}\n")
        file.write(f"Normalization: {self.con.normalization}\n\n")

        file.write(f"{self.con.network_type} measures:\n\n")
        for attribute, value in self.con.network_measures[self.con.network_type].items():
            file.write(f"\t{attribute}: " + str(value) + "\n")
        return
        

    def get_person_and_day_ratios(self, data_splits, data_sets):
        ratios = {}
        fold = 0
        for data_split in data_splits:
            ratios[fold] = {}
            for data_type in ["training", "validation"]:
                ratios[fold][data_type] = {}
                person_ratios = {}
                day_ratios = {}
                for data_set_name in data_split[data_type]["data_sets"]:
                    person = data_sets[data_set_name]["person-label"]
                    day = "_".join(data_set_name.split("_")[1:3])
                    tabs = "\t\t"
                    if len(person) >= 8: tabs = "\t"
                    cut_ratio = data_split["cut_ratios"]["person"][data_type] if not person == "nobody" else data_split["cut_ratios"]["nobody"][data_type]
                    ratio_in_packets = round(data_sets[data_set_name]["length"] * cut_ratio) / data_split[data_type]["packets"]
                    person_ratios.setdefault(person, 0)
                    day_ratios.setdefault(day, 0)
                    person_ratios[person] += ratio_in_packets
                    day_ratios[day] += ratio_in_packets

                ratios[fold][data_type]["person_ratios"] = person_ratios
                ratios[fold][data_type]["day_ratios"] = day_ratios
            fold += 1

        return ratios


    def write_data_split_measures(self, file, data_splits, data_sets):
        fold = 0
        for data_split in data_splits:
            fold += 1
            file.write(f"\n\n----------------------------- Split for Fold {fold} -----------------------------\n")
            file.write("\ntrain/validation-ratio: " + str(data_split["train/validation-ratio"]) + "\n")
            for data_type in ["training", "validation"]:
                person_ratios = {}
                day_ratios = {}
                file.write(f"\n{data_type} measures:\n")
                file.write("\tdata sets:\n")
                for data_set_name in data_split[data_type]["data_sets"]:
                    person = data_sets[data_set_name]["person-label"]
                    day = "_".join(data_set_name.split("_")[1:3])
                    tabs = "\t\t"
                    if len(person) >= 8: tabs = "\t"
                    cut_ratio = data_split["cut_ratios"]["person"][data_type] if not person == "nobody" else data_split["cut_ratios"]["nobody"][data_type]
                    ratio_in_packets = round(data_sets[data_set_name]["length"] * cut_ratio) / data_split[data_type]["packets"]
                    person_ratios.setdefault(person, 0)
                    day_ratios.setdefault(day, 0)
                    person_ratios[person] += ratio_in_packets
                    day_ratios[day] += ratio_in_packets
                    file.write("\t\t" + data_set_name + ",\t" + person 
                                + tabs + "--> " + "{:.1%}".format(cut_ratio) +  " used, \t" 
                                + "{:.1%}".format(ratio_in_packets) + "\n")
                
                file.write("\n\tdata distribution:\n")
                file.write("\t\tpersons:\n")
                for person, ratio in person_ratios.items():
                    tabs = "\t\t"
                    if len(person) >= 7: tabs = "\t"
                    file.write("\t\t\t" + person + f":{tabs}" + "{:.1%}".format(ratio) + "\n")

                file.write("\n\t\tdays:\n")
                for day, ratio in day_ratios.items():
                    file.write("\t\t\t" + day + f":\t" + "{:.1%}".format(ratio) + "\n")
                
        return
    

    def save_loss_curve(self, clf, dir_name, fold):
        filepath = os.path.join(self.con.results_folder, dir_name, "curves.png")
        if self.con.network_type == 'MLP':
            plt.plot(clf.loss_curve_, label=f"loss_{fold}")
            plt.legend()
            plt.savefig(filepath)
            if self.con.network_measures["MLP"]["early_stopping"]:
                plt.plot(clf.validation_scores_, label=f"score_{fold}")
                plt.legend()
                plt.savefig(filepath)


    def write_fold_measures(self, clf, file, fold, data_split, y_pred):
        cm = metrics.confusion_matrix(data_split["validation"]["y"], y_pred, labels=clf.classes_)
        
        file.write(f"\n--------------- Fold {fold+1} ---------------\n")
        file.write("\n  Accuracy:\t" + "{:.2%}".format(metrics.accuracy_score(data_split["validation"]["y"], y_pred)) + "\n")
        file.write("  Precision:\t" + "{:.2%}".format(metrics.precision_score(data_split["validation"]["y"], y_pred)) + "\n")
        file.write("  Recall:\t" + "{:.2%}".format(metrics.recall_score(data_split["validation"]["y"], y_pred)) + "\n")
        file.write("  F1:\t\t" + "{:.2%}".format(metrics.f1_score(data_split["validation"]["y"], y_pred)) + "\n")
        file.write("\n  TN: " + "{:.2%}".format(cm[0][0] / y_pred.shape[0]) + "\n")
        file.write("  FN: " + "{:.2%}".format(cm[1][0] / y_pred.shape[0]) + "\n")
        file.write("  TP: " + "{:.2%}".format(cm[1][1] / y_pred.shape[0]) + "\n")
        file.write("  FP: " + "{:.2%}".format(cm[0][1] / y_pred.shape[0]) + "\n")
        file.write("---------------------------------------\n")


    def print_result(self, clf, y_test, y_pred, fold):
        #cm = metrics.confusion_matrix(y_test, y_pred, labels=clf.classes_)
        #disp = metrics.ConfusionMatrixDisplay(confusion_matrix=cm, display_labels=clf.classes_)
        #disp.plot()
        
        print(f"--------------- Fold {fold+1} ---------------")
        print("Accuracy:", metrics.accuracy_score(y_test, y_pred))
        print("Precision:", metrics.precision_score(y_test, y_pred))
        print("Recall:", metrics.recall_score(y_test, y_pred))
        print("F1:", metrics.f1_score(y_test, y_pred))
        print("----------------------------------------------\n")
