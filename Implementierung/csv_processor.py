from config import Config
import pandas as pd
import os


class CSV:
    def __init__(self):
        self.con = Config()
        self.CSI_data_sets = dict()
        self.num_timestamps = 0


    def get_csv_file_list(self):
        files_list = []
        [files_list.append(file) for file in os.listdir(self.con.csv_folder) if '.csv' in file[-4:]]
        return files_list


    def read_csi_data(self, csv_files):
        for csv_file in csv_files:
            csv_data_frame = pd.read_csv(os.path.join(self.con.csv_folder, csv_file), sep=';')
            csv_dict = csv_data_frame.to_dict()
            self.num_timestamps = int(len(csv_dict["timestamp"]) / self.con.num_subcarriers)
            self.fill_csi_data_set(csv_dict, csv_file)
        return self.CSI_data_sets


    def fill_csi_data_set(self, csv_dict, csv_file):
        amplitudes = self.get_amplitudes_per_timestamp(csv_dict)
        timestamps = self.get_timestamps(csv_dict)
        csi_file_dict = {"data": dict()}
        for timestamp in timestamps:
            csi_data_for_time = amplitudes[timestamps.index(timestamp)]
            csi_file_dict["data"][timestamp] = dict()
            csi_file_dict["data"][timestamp]["amplitudes"] = csi_data_for_time

        self.CSI_data_sets[csv_file.split(".")[0]] = csi_file_dict


    def get_timestamps(self, csv_dict):
        timestamps = []
        [timestamps.append(csv_dict["timestamp"][(index * self.con.num_subcarriers)])
            for index in range(self.num_timestamps)]
        return timestamps


    def get_amplitudes_per_timestamp(self, csv_dict):
        amplitudes_total, amplitudes_per_timestamp = [], []
        [amplitudes_total.append(csv_dict["amplitude"][index]) for index in list(csv_dict["amplitude"])]
        for timestamp in range(self.num_timestamps):
            amplitudes_for_timestamp = []
            [amplitudes_for_timestamp.append(
                float(csv_dict["amplitude"][timestamp * self.con.num_subcarriers + subcarrier_index].replace(',', '.'))) for subcarrier_index
                in range(self.con.num_subcarriers) if subcarrier_index not in self.con.cross_out_subcarriers]
            amplitudes_per_timestamp.append(amplitudes_for_timestamp)

        return amplitudes_per_timestamp
