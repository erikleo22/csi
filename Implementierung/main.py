from trainer import Trainer
from label_processor import LabelProcessor
from sensor_processor import Sensor
from caching import Cache
from preprocessor import Preprocessor


def main():
    label_processor = LabelProcessor()
    model_trainer = Trainer()
    sensor = Sensor()
    cache = Cache()

    csi_data_sets = cache.receive_csi_data_sets()
    label_data_sets = label_processor.read_label_files()
    sensor_data_set = sensor.read_sensor_data_file()

    data_and_labels = merge_data_and_labels(cache, csi_data_sets, label_data_sets, sensor_data_set)

    model_trainer.train_model(data_and_labels)


def merge_data_and_labels(cache, csi_data_sets, label_data_sets, sensor_data_set):
    without_person, with_person = [0,0], [0,0]
    for data_set_name, label_dict in label_data_sets.items():
        csi_data_set = csi_data_sets[data_set_name]
        csi_data_set = cache.get_csi_and_sensor_data(data_set_name, csi_data_set, sensor_data_set, label_dict["person-position"])
        label_data_sets[data_set_name].update(csi_data_set)
        length = len(csi_data_set["data"])
        label_data_sets[data_set_name]["length"] = length
        if("nside" in label_dict["person-position"]): 
            with_person[0] += 1
            with_person[1] += length
        else: 
            without_person[0] += 1
            without_person[1] += length
        
    #print(f"With person: {with_person[0]} datasets with {with_person[1]} packets.")
    #print(f"Empty: {without_person[0]} datasets with {without_person[1]} packets.")
    return label_data_sets


main()