import tkinter as tk
import json

# Define the directory to monitor
directory_to_watch = "/etc/CSI/WiFiEye/studio"

def save_to_file(person_label, temperature, humidity, filename):
    data = {
        "person-label": person_label,
        "temperature": temperature,
        "humidity": humidity
    }
    filename = filename.split(".")[0] + ".txt"
    try:
        with open(filename, 'x') as file:
            json.dump(data, file, indent=3)

    except Exception:
        print("File already exists")
        return

    print(f"Data saved to {filename}")

def on_file_added(event):
    new_file = event.src_path
    # Open the GUI and get user input
    if ".csv" in new_file[-4:]:
        print(f"New file added: {new_file}")
        open_gui(new_file)

def open_gui(new_file):
    window = tk.Tk()
    window.title("Data Input")

    # Function to handle the save button click
    def save_button_click():
        person_label = person_label_entry.get()
        temperature = temperature_entry.get()
        humidity = humidity_entry.get()

        save_to_file(person_label, temperature, humidity, new_file)

        window.destroy()

    # GUI elements
    person_label_label = tk.Label(window, text="Person Label:")
    person_label_entry = tk.Entry(window)

    temperature_label = tk.Label(window, text="Temperature in °C:")
    temperature_entry = tk.Entry(window)

    humidity_label = tk.Label(window, text="Humidity in %:")
    humidity_entry = tk.Entry(window)

    save_button = tk.Button(window, text="Save", command=save_button_click)

    # Layout
    person_label_label.grid(row=0, column=0, sticky="e")
    person_label_entry.grid(row=0, column=1)

    temperature_label.grid(row=1, column=0, sticky="e")
    temperature_entry.grid(row=1, column=1)

    humidity_label.grid(row=2, column=0, sticky="e")
    humidity_entry.grid(row=2, column=1)

    save_button.grid(row=3, column=0, columnspan=3, pady=20, padx=100)

    window.mainloop()

# Set up a file system monitor to detect file additions
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

class MyHandler(FileSystemEventHandler):
    def on_created(self, event):
        if event.is_directory:
            return
        on_file_added(event)


# Set up the file system observer
event_handler = MyHandler()
observer = Observer()
observer.schedule(event_handler, path=directory_to_watch, recursive=False)
observer.start()

try:
    while True:
        pass
except KeyboardInterrupt:
    observer.stop()

observer.join()
