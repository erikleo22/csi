import warnings
import pandas as pd
import os
import sys
from datetime import datetime
from multiprocessing.pool import ThreadPool
import numpy as np

'''
df = pd.DataFrame()
df._set_value(1, col=1, value=1.0)
df._set_value(1, col=2, value=2.0)
df._set_value(1, col=3, value=3.0)

df._set_value(2, col=1, value=4.0)
df._set_value(2, col=2, value=5.0)
df._set_value(2, col=3, value=6.0)
amplitudes = df._values

mean_amplitudes = pd.DataFrame()
col=0
for packet_amplitudes in amplitudes:
    mean_amplitudes._set_value(0, col=col, value=np.mean(packet_amplitudes))
    col+=1

mean_amplitude_for_window = np.mean(mean_amplitudes._values)
'''

def classify(df_realtime):
    #with open("/home/alejandro/Documents/CSI/Implementierung/wifieye_buffer_out.txt", "a") as f:
    #    f.write("DataFrame:\n")
    #    f.write(str(df_realtime) + "\n")
    #    f.close()

    amplitudes = df_realtime._values
    mean_amplitudes = pd.DataFrame()
    col=0
    for packet_amplitudes in amplitudes:
        mean_amplitudes._set_value(0, col=col, value=np.mean(packet_amplitudes))
        col+=1

    mean_amplitude_for_window = np.mean(mean_amplitudes._values)

    predicted_label = 0
    if mean_amplitude_for_window > 0.2:
        predicted_label = 1

    # Log to console
    print(f"Amplitude: {mean_amplitude_for_window}, predicted: {predicted_label}")
    # print(f'{current_time} --> {labels[pred_label]} (acc: {round(pred_porb, 2)})', file=sys.stderr)

    #with open("/home/alejandro/Documents/CSI/Implementierung/classification_result.txt", "a") as f:
    #    f.write(f"Amplitude: {mean_amplitude_for_window}, predicted: {predicted_label}")
    #    f.close()

    return str(':'.join([str(predicted_label), str(1.0)]))


if __name__ == '__main__':
    # cross out subcarriers
    num_subcarriers = 128
    null_subcarriers = [0,1,2,3,4,5,63,64,65,123,124,125,126,127]
    pilot_subcarriers = [11,39,53,75,89,117]
    manual_co_subcarriers = list(range(64,127))
    cross_out_subcarriers = list(set(null_subcarriers + pilot_subcarriers + manual_co_subcarriers))

    # Settings
    buffer = ''
    separator = ';'
    sampling_frequency = 10  # Hz
    time_window_size = sampling_frequency * 0.5  # s
    pool = ThreadPool(processes=1)
    df_realtime = pd.DataFrame()

    #with open("/home/alejandro/Documents/CSI/Implementierung/classification_result.txt", "x") as f:
    #    f.write("\n")
    #    f.close()

    sys.stdout.buffer.write(bytes([0xca, 0xff, 0xee]))  # magic number
    sys.stdout.flush()

    while True:
        # Keep reading from stdin
        buffer += sys.stdin.readline()

        # Check if line is ready
        if buffer.endswith('\n'):
            line = buffer[:-1]
            buffer = ''
            #with open("/home/alejandro/Documents/CSI/Implementierung/wifieye_buffer_out.txt", "a") as f:
            #    f.write(str(line) + "\n")
            #    f.close()

            # Add CSI amplitude value to create the sample to feed into the model
            time, mac, sub, amp, phase, rssi, fc = str(line).split(';')
            if sub not in cross_out_subcarriers:
                time = pd.to_datetime(str(pd.to_datetime(':'.join(time.split(':')[:-1]) + '.' + time.split(':')[-1])))
                df_realtime._set_value(time, col=f'sub ({sub})', value=float(amp.replace(',', '.')))

            # If the realtime data shape match the input shape required by the model then perform classification
            if df_realtime.shape[0] == time_window_size and not df_realtime.isnull().values.any():
                df_realtime = df_realtime.values.reshape((1, df_realtime.shape[0], df_realtime.shape[1]))

                # Prediction
                async_result = pool.apply_async(classify, df_realtime)

                # Output prediction to stdout
                print('%s' % async_result.get())
                sys.stdout.flush()
                df_realtime = pd.DataFrame()
