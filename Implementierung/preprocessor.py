from config import Config
from logger import Logger
import numpy as np
from sklearn import utils
from sklearn.preprocessing import normalize
import itertools
import pywt
from matplotlib import pyplot as plt
import os

class Preprocessor:
    def __init__(self):
        self.con = Config()
        self.logger = Logger()

    
    #def data_preprocessing(self, csi_data_sets):
    #    csi_data_sets = self.remove_outliers(csi_data_sets)
    #    return csi_data_sets


    def remove_outliers(self, dir_name, data_split):
        window_neighbors = int((self.con.outlier_removal_window - 1) / 2) # number of neighbors inside window
        filepath = os.path.join(self.con.results_folder, dir_name, "data")
        plt.clf()
        # save raw data to file
        for set_type in ["training", "validation"]:
            filepath_raw = filepath + "_raw.png"
            for packet_index, packet in enumerate(data_split[set_type]["X"]):
                if packet_index >= 100: break
                plt.plot(packet)
                plt.savefig(filepath_raw)

        plt.clf()

        for set_type in ["training", "validation"]:
            filepath_rm = filepath + "_rm_outliers_.png"
            for packet_index, packet in enumerate(data_split[set_type]["X"]):
                for index in range(window_neighbors, packet.shape[0] - window_neighbors - 1):
                    window = packet[(index-window_neighbors):(index+window_neighbors + 1)]
                    std_dev = np.std(window)
                    mean = np.mean(window)
                    threshold_max = round(mean + self.con.outlier_removal_threshold * std_dev, 10)
                    threshold_min = round(mean - self.con.outlier_removal_threshold * std_dev, 10)
                    for win_index, amplitude_value in enumerate(window):
                        if amplitude_value > threshold_max:
                            packet[index - window_neighbors + win_index] = threshold_max
                        elif amplitude_value < threshold_min:
                            packet[index - window_neighbors + win_index] = threshold_min

                if packet_index < self.con.max_num_packets_logging:
                    plt.plot(packet)
                    plt.savefig(filepath_rm)
                
        return data_split
    

    def remove_outliers_data_sets(self, csi_data_set):
        window_neighbors = int((self.con.outlier_removal_window - 1) / 2) # number of neighbors inside window
        for time_stamp, amplitude_data in csi_data_set["data"].items():
            for index in range(window_neighbors, len(amplitude_data["amplitudes"]) - window_neighbors - 1):
                window = amplitude_data["amplitudes"][(index - window_neighbors) : (index + window_neighbors + 1)]
                window_array = np.array(window)
                std_dev = np.std(window_array)
                mean = np.mean(window_array)
                threshold_max = round(mean + self.con.outlier_removal_threshold * std_dev, 10)
                threshold_min = round(mean - self.con.outlier_removal_threshold * std_dev, 10)
                for win_index, amplitude_value in enumerate(window):
                    if amplitude_value > threshold_max:
                        amplitude_data["amplitudes"][index - window_neighbors + win_index] = threshold_max
                    elif amplitude_value < threshold_min:
                        amplitude_data["amplitudes"][index - window_neighbors + win_index] = threshold_min

        return csi_data_set


    def perform_dwt_data_sets(self, csi_data_set):
        for time_stamp, amplitudes_dict in csi_data_set["data"].items():
            (cA, cD) = pywt.dwt(amplitudes_dict["amplitudes"], 'db2')
            csi_data_set["data"][time_stamp]["amplitudes"] = list(cA)
        return csi_data_set


    def perform_dwt(self, dir_name, data_split):
        filepath = os.path.join(self.con.results_folder, dir_name, "dwt_")
        for set_type in ["training", "validation"]:
            filepath_curr = filepath + set_type + ".png"
            for packet, packet_index in enumerate(data_split[set_type]["X"]):
                (cA, cD) = pywt.dwt(packet, 'db2')
                if packet_index < self.con.max_num_packets_logging:
                    plt.plot(cA)
                    plt.savefig(filepath_curr)
                else: return
                #plt.plot(cA)
                #plt.show()
                #pass
            #plt.show()


    def get_data_split(self, data_sets, splits):
        data_set_splits = self.get_data_set_split_names(data_sets, splits)
        data_splits = []

        for split_index, data_split in enumerate(data_set_splits):
            X_train, X_test, y_train, y_test = np.array([]), np.array([]), np.array([]), np.array([])

            training_data_set_names = {
                "person": ["_".join(set_name.split("_")[:-1]) for set_name in data_split["training"] if not "nobody" in set_name],
                "nobody": ["_".join(set_name.split("_")[:-1]) for set_name in data_split["training"] if "nobody" in set_name]
            }
            validation_data_set_names = {
                "person": ["_".join(set_name.split("_")[:-1]) for set_name in data_split["validation"] if not "nobody" in set_name],
                "nobody": ["_".join(set_name.split("_")[:-1]) for set_name in data_split["validation"] if "nobody" in set_name]
            }

            num_training_packets_person_uncut = num_training_packets_cut = sum(data_sets[training_data_set_name]["length"] for training_data_set_name in training_data_set_names["person"])
            num_validation_packets_person_uncut = num_validation_packets_cut = sum(data_sets[validation_data_set_name]["length"] for validation_data_set_name in validation_data_set_names["person"])
            num_training_packets_nobody = sum(data_sets[training_data_set_name]["length"] for training_data_set_name in training_data_set_names["nobody"])
            num_validation_packets_nobody = sum(data_sets[validation_data_set_name]["length"] for validation_data_set_name in validation_data_set_names["nobody"])

            training_ratio = num_training_packets_person_uncut / (num_training_packets_person_uncut + num_validation_packets_person_uncut)

            if training_ratio < self.con.training_data_percentage_min:
                sum_of_packets = int(num_training_packets_person_uncut / self.con.training_data_percentage_min)
                num_validation_packets_cut = sum_of_packets - num_training_packets_person_uncut

            elif training_ratio > self.con.training_data_percentage_max:
                sum_of_packets = int(num_validation_packets_person_uncut / (1 - self.con.training_data_percentage_max))
                num_training_packets_cut = sum_of_packets - num_validation_packets_person_uncut

            training_ratio = num_training_packets_cut / (num_training_packets_cut + num_validation_packets_cut)

            cut_ratios = {
                "person":{
                    "training": num_training_packets_cut / num_training_packets_person_uncut,
                    "validation": num_validation_packets_cut / num_validation_packets_person_uncut
                },
                "nobody":{
                    "training": num_training_packets_cut / num_training_packets_nobody,
                    "validation": num_validation_packets_cut / num_validation_packets_nobody
                }
            }

            for data_set_type, ratio in cut_ratios["nobody"].items(): 
                if ratio > 1: raise(Exception(f"Not enough data without person for {data_set_type} sets. Ratio {str(ratio)}"))

            X_train, y_train = self.get_data_and_labels(data_sets, "training", training_data_set_names, cut_ratios, X_train, y_train)
            X_test, y_test = self.get_data_and_labels(data_sets, "validation", validation_data_set_names, cut_ratios, X_test, y_test)

            if split_index == 0:
                self.logger.print_average_data_of_person_types(self.con.logging_dir_name, 
                                                        X_train, 
                                                        y_train,
                                                        "raw_data")

            if self.con.normalization:
                X_train = normalize(X_train, 'max')
                X_test = normalize(X_test, 'max')

                if split_index == 0:
                    self.logger.print_average_data_of_person_types(self.con.logging_dir_name, 
                                                            X_train, 
                                                            y_train,
                                                            "normalized_raw_data")

            zero_rate_test = np.nonzero(y_test)[0].shape[0] / y_test.shape[0]
            zero_rate_training = np.nonzero(y_train)[0].shape[0] / y_train.shape[0]

            data_splits.append({
                "training":{
                    "X": X_train,
                    "y": y_train,
                    "data_sets": training_data_set_names["person"] + training_data_set_names["nobody"],
                    "persons": data_split["training_persons"],
                    "packets": y_train.shape[0]
                },
                "validation":{
                    "X": X_test,
                    "y": y_test,
                    "data_sets": validation_data_set_names["person"] + validation_data_set_names["nobody"],
                    "persons": data_split["validation_persons"],
                    "packets": y_test.shape[0]
                },
                "train/validation-ratio": "{:.1%}".format(training_ratio) + " / " + "{:.1%}".format(1-training_ratio),
                "cut_ratios": cut_ratios
            })

        return data_splits


    def get_data_and_labels(self, data_sets, data_type, data_set_names, cut_ratios, X, y):
        for person_type, training_set_names in data_set_names.items():
            for training_set_name in training_set_names:
                data_set = data_sets[training_set_name]
                stop_index = int(cut_ratios[person_type][data_type] * data_set["length"])
                X,y = self.get_np_array_from_data_set(X, y, data_set, stop_index)

        return X,y


    def get_data_set_split_names(self, data_sets, desired_amount):
        if self.con.split_mode == "manual":
            return [dict({
                "training": [training_set_name + "_" + data_sets[training_set_name]["person-label"] 
                             for person_type in ["with person", "without person"]
                             for training_set_name in self.con.training_manual[person_type]], 
                "validation": [validation_set_name + "_" + data_sets[validation_set_name]["person-label"] 
                             for person_type in ["with person", "without person"]
                             for validation_set_name in self.con.validation_manual[person_type]], 
                "training_persons": list(set([data_sets[training_set_name]["person-label"] 
                                    for person_type in ["with person", "without person"]
                                    for training_set_name in self.con.training_manual[person_type]])),
                "validation_persons": list(set([data_sets[validation_set_name]["person-label"] 
                                    for person_type in ["with person", "without person"]
                                    for validation_set_name in self.con.validation_manual[person_type]]))
            })]

        candidates_with_person = self.get_split_candidates("with person", data_sets)
        candidates_without_person = self.get_split_candidates("without person", data_sets)

        validation_persons_candidates = dict(sorted(candidates_with_person["validation"]["persons"].items(), key=lambda x:x[1], reverse=True))
        data_sets_with_persons = set(candidates_with_person["validation"]["data_sets"] + candidates_with_person["training"]["data_sets"])
        valid_pairs = self.get_valid_pairs_of_sets(data_sets_with_persons)

        training_combinations = list(itertools.combinations(list(candidates_with_person["training"]["persons"]), min(5,len(candidates_with_person["training"]["persons"]))-1))
        validation_combinations = list(itertools.combinations(list(candidates_with_person["validation"]["persons"]), min(2,len(candidates_with_person["validation"]["persons"]))))

        chosen_splits = []
        for valid_pair in valid_pairs:
            if len(chosen_splits) >= desired_amount:
                break
            (amount_training_sets, amount_validation_sets, perc) = valid_pair
            for training_persons in training_combinations:
                possible_training_amount = self.count_possible_sets_for_persons(training_persons, candidates_with_person["training"]["persons"])
                if possible_training_amount >= amount_training_sets:
                    for validation_persons in validation_combinations:
                        #if self.check_if_existing_in_chosen_splits(chosen_splits, validation_persons, "validation"):
                        #    continue
                        remaining_validation_persons = set(validation_persons) - set(training_persons)
                        possible_validation_amount = self.count_possible_sets_for_persons(
                            remaining_validation_persons, validation_persons_candidates)
                        if possible_validation_amount == amount_validation_sets:
                            #if validation_persons in chosen_splits
                            chosen_splits.append([training_persons, validation_persons])
                            break  
                
        return self.get_split_data_set_names(chosen_splits, desired_amount, candidates_with_person, candidates_without_person)


    def check_if_existing_in_chosen_splits(self, chosen_splits, persons, data_set_type):
        for chosen_split in chosen_splits:
            if data_set_type == "training":
                if chosen_split[0] == persons: return True

            if data_set_type == "validation":
                if chosen_split[1] == persons: return True

        return False



    def get_split_candidates(self, label, data_sets):
        validation_candidates, training_candidates = [], []
        validation_persons_candidates, training_persons_candidates = dict(), dict()
        for data_set_name, data_set in data_sets.items():
            person = data_set["person-label"]
            if self.con.split_mode == "days":
                type_dict = self.determine_data_set_type(data_set_name, label)
            elif self.con.split_mode == "persons":
                type_dict = self.determine_data_set_type(person, label)
            
            if ("obody" in person) & (label == "with person"):
                continue
            elif ("obody" not in person) & (label == "without person"):
                continue
            if type_dict["validation"]:
                #if not data_set_name in ["CSI_March_18_24_15_17_18","CSI_March_18_24_15_23_38", "CSI_March_18_24_15_20_30"]:
                validation_candidates.append(data_set_name + "_" + person)
                validation_persons_candidates.setdefault(person, 0)
                validation_persons_candidates[person] += 1
            if type_dict["training"]:
                #if not data_set_name in ["CSI_March_18_24_15_30_28", "CSI_March_18_24_15_33_27"]:
                training_candidates.append(data_set_name + "_" + person)
                training_persons_candidates.setdefault(person, 0)
                training_persons_candidates[person] += 1
            
        return {"validation": {"data_sets": validation_candidates, "persons": validation_persons_candidates},
                "training": {"data_sets": training_candidates, "persons": training_persons_candidates}}


    def determine_data_set_type(self, data_set_name, label):
        if self.con.split_mode == "days":
            training = [True for training_day in self.con.training_days[label] if training_day in data_set_name]
            validation = [True for validation_day in self.con.validation_days[label] if validation_day in data_set_name]
        elif self.con.split_mode == "persons":
            training = [True for training_person in self.con.training_persons[label] if training_person in data_set_name]
            validation = [True for validation_person in self.con.validation_persons[label] if validation_person in data_set_name]
        
        if training == []: training = [False]
        if validation == []: validation = [False]
        return {"validation": validation[0], "training": training[0]}


    def get_split_data_set_names(self, chosen_splits, desired_amount, candidates_with_person, candidates_without_person):
        pair_index = 0
        split_data_sets = []
        for chosen_split in chosen_splits:
            if pair_index >= desired_amount:
                break
            
            (training_persons, validation_persons) = chosen_split
            training_sets = [training_set for training_set in candidates_with_person["training"]["data_sets"] 
                             if training_set.split("_")[-1] in training_persons]
            validation_sets = [validation_set for validation_set in candidates_with_person["validation"]["data_sets"] 
                             if validation_set.split("_")[-1] in validation_persons]
        
            [training_sets.append(candidate_without_person) for candidate_without_person in candidates_without_person["training"]["data_sets"]]
            [validation_sets.append(candidate_without_person) for candidate_without_person in candidates_without_person["validation"]["data_sets"]]
            
            split_data_sets.append({
                "training": training_sets, 
                "validation": validation_sets, 
                "training_persons": training_persons,
                "validation_persons": validation_persons
            })
            pair_index += 1

        return split_data_sets


    def get_valid_pairs_of_sets(self, data_sets_with_persons):
        valid_pairs = []
        for amount in range(int(len(data_sets_with_persons)), 0, -1):
            one_set_percentage = 1 / amount # one set could be cut
            amount_training_sets_min = int(self.con.training_data_percentage_min * amount)
            amount_training_sets_max = int(self.con.training_data_percentage_max * amount)
            amount_validation_sets_min = amount - amount_training_sets_min
            amount_validation_sets_max = amount - amount_training_sets_max
            
            if amount_training_sets_max / amount < self.con.training_data_percentage_max + one_set_percentage:
                valid_pairs.append([amount_training_sets_max, amount_validation_sets_max, str(amount_training_sets_max/amount)])
            if amount_training_sets_min / amount >= self.con.training_data_percentage_min - one_set_percentage:
                valid_pairs.append([amount_training_sets_min, amount_validation_sets_min, str(amount_training_sets_min/amount)])
        return valid_pairs


    def count_possible_sets_for_persons(self, persons, persons_candidates):
        amount_sets = 0
        for person in persons:
            amount_sets += persons_candidates[person]
        return amount_sets
                    

    def get_np_array_from_data_set(self, X, y, data_set, stop_index):
        #X, y = utils.shuffle(X, y)
        data_array = np.array([np.array(time_stamp_data["amplitudes"])
                                        for time_stamp, time_stamp_data in data_set["data"].items()])
        label_array = np.array([np.array(0 if "obody" in data_set["person-label"] else 1)
                for time_stamp, time_stamp_data in data_set["data"].items()])
        
        if self.con.use_packet_windows:
            mean_array = np.array([np.mean(data_array[index : (index + self.con.packet_window)], axis=0) 
                        for index in range(stop_index - self.con.packet_window)])
            X = mean_array if X.shape[0] == 0 else np.append(X, mean_array, axis=0)
            y = label_array[:(stop_index - self.con.packet_window)] if y.shape[0] == 0 else np.append(y, label_array[:(stop_index - self.con.packet_window)], axis=0)
        
        else:  
            X = data_array[:stop_index] if X.shape[0] == 0 else np.append(X, data_array[:stop_index], axis=0)
            y = label_array[:stop_index] if y.shape[0] == 0 else np.append(y, label_array[:stop_index], axis=0)
        return (X, y)



############################################################################################
######################################## deprecated ########################################
############################################################################################


    def rearrange_data(self, data_sets):
        data = np.array([np.array(time_stamp_data["amplitudes"]) 
                          for data_set_name, data_set in data_sets.items() 
                          for time_stamp, time_stamp_data in data_set["data"].items()])
        
        labels = np.array([np.array(0 if "obody" in data_set["person-label"] else 1) 
                          for data_set_name, data_set in data_sets.items()
                          for time_stamp, time_stamp_data in data_set["data"].items()])

        return (data, labels)
