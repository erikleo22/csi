import tkinter as tk
import json
from datetime import datetime
import os

# Define the directory to monitor
directory_to_save = "C:\\Don Leo lokal\\Forschungspraktikum_CSI"

def save_to_file(front_left_id, front_right_id, rear_left_id, rear_right_id):
    data = {
        "front_right_id": front_right_id,
        "front_left_id": front_left_id,
        "rear_right_id": rear_right_id,
        "rear_left_id": rear_left_id
    }
    now_str = datetime.now().strftime("%Y_%m_%d-%H_%M_%S")
    filename = os.path.join(directory_to_save, now_str + ".txt")
    try:
        with open(filename, 'x') as file:
            json.dump(data, file, indent=3)

    except Exception:
        print("File already exists")
        return

    print(f"Data saved to {filename}")


def open_gui():
    window = tk.Tk()
    window.title("Data Input")

    # Function to handle the save button click
    def save_button_click():
        front_left_id = front_left_entry.get()
        front_right_id = front_right_entry.get()
        rear_left_id = rear_left_entry.get()
        rear_right_id = rear_right_entry.get()

        save_to_file(front_left_id, front_right_id, rear_left_id, rear_right_id)

        window.destroy()

    # GUI elements
    front_left_id = tk.Label(window, text="ID vorne links")
    front_left_entry = tk.Entry(window)

    front_right_id = tk.Label(window, text="ID vorne rechts")
    front_right_entry = tk.Entry(window)

    rear_left_id = tk.Label(window, text="ID hinten links")
    rear_left_entry = tk.Entry(window)

    rear_right_id = tk.Label(window, text="ID hinten rechts")
    rear_right_entry = tk.Entry(window)

    save_button = tk.Button(window, text="Save", command=save_button_click)

    # Layout
    front_left_id.grid(row=0, column=0, sticky="nesw")
    front_left_entry.grid(row=1, column=0)

    front_right_id.grid(row=0, column=1, sticky="nesw")
    front_right_entry.grid(row=1, column=1)

    rear_left_id.grid(row=3, column=0, sticky="nesw")
    rear_left_entry.grid(row=2, column=0)

    rear_right_id.grid(row=3, column=1, sticky="nesw")
    rear_right_entry.grid(row=2, column=1)

    save_button.grid(row=4, column=0, columnspan=3, pady=20, padx=100)

    window.mainloop()


open_gui()
