from config import Config
import pandas as pd
from datetime import datetime

class Sensor():
    def __init__(self):
        self.con = Config()
        self.sensor_data = dict()


    def read_sensor_data_file(self):
        sensor_data_frame = pd.read_csv(self.con.temp_humidity_file, sep=',')
        self.rearrange_sensor_data(sensor_data_frame.to_dict())
        return self.sensor_data


    def rearrange_sensor_data(self, sensor_data_dict):
        new_sensor_dict = dict()
        for index, timestamp in sensor_data_dict['time'].items():
            new_sensor_dict[timestamp] = dict({
                "humidity": sensor_data_dict['humidity'][index][:-1],
                "temperature": sensor_data_dict['temperature'][index][:-4]
            })
        self.sensor_data = new_sensor_dict


    def match_sensor_data(self, csi_data_set, sensor_data_set):
        current_temp, current_hum = None, None
        same_sensor_point_counter = 0
        for csi_time_stamp, csi_data in csi_data_set["data"].items():
            if(same_sensor_point_counter > 0):
                csi_data_set["data"][csi_time_stamp]["temperature"] = current_temp
                csi_data_set["data"][csi_time_stamp]["humidity"] = current_hum
                same_sensor_point_counter -= 1
                continue

            csi_time_datetime = datetime.strptime(csi_time_stamp, "%Y-%m-%d %H:%M:%S:%f")
            for sensor_time_stamp, sensor_data in sensor_data_set.items():
                sensor_time_datetime = datetime.strptime(sensor_time_stamp, "%Y-%m-%d_%H:%M:%S")
                time_distance_1 = csi_time_datetime - sensor_time_datetime
                time_distance_2 = sensor_time_datetime - csi_time_datetime
                if (time_distance_1.seconds <= 4) | (time_distance_2.seconds <= 4):
                    csi_data_set["data"][csi_time_stamp]["temperature"] = sensor_data["temperature"]
                    csi_data_set["data"][csi_time_stamp]["humidity"] = sensor_data["humidity"]
                    current_temp = sensor_data["temperature"]
                    current_hum = sensor_data["humidity"]
                    if time_distance_1.seconds < time_distance_2.seconds:
                        same_sensor_point_counter = (4 - time_distance_1.seconds) * 10
                    else:
                        same_sensor_point_counter = (5 + time_distance_2.seconds) * 10
                    break
                elif (time_distance_1.seconds <= 10) | (time_distance_2.seconds <= 10):
                    csi_data_set["data"][csi_time_stamp]["temperature"] = current_temp
                    csi_data_set["data"][csi_time_stamp]["humidity"] = current_hum

        return csi_data_set
    