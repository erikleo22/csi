Pre-processing:

Outlier Removal: True
Discrete Wavelet: True
Normalization: False

SVM measures:

	C: 10
	kernel: poly
	degree: 3
	gamma: 0.1
	verbose: True

--------------- Fold 1 ---------------

  Accuracy:	78.39%
  Precision:	79.25%
  Recall:	76.94%
  F1:		78.07%

  TN: 39.92%
  FN: 11.53%
  TP: 38.47%
  FP: 10.08%
---------------------------------------

--------------- Fold 2 ---------------

  Accuracy:	77.11%
  Precision:	78.11%
  Recall:	75.34%
  F1:		76.70%

  TN: 39.44%
  FN: 12.33%
  TP: 37.67%
  FP: 10.56%
---------------------------------------

--------------- Fold 3 ---------------

  Accuracy:	77.77%
  Precision:	78.36%
  Recall:	76.76%
  F1:		77.55%

  TN: 39.38%
  FN: 11.63%
  TP: 38.39%
  FP: 10.60%
---------------------------------------

--------------- Fold 4 ---------------

  Accuracy:	85.51%
  Precision:	81.97%
  Recall:	91.07%
  F1:		86.28%

  TN: 39.95%
  FN: 4.47%
  TP: 45.57%
  FP: 10.02%
---------------------------------------

--------------- Fold 5 ---------------

  Accuracy:	85.47%
  Precision:	81.26%
  Recall:	92.24%
  F1:		86.40%

  TN: 39.33%
  FN: 3.88%
  TP: 46.15%
  FP: 10.64%
---------------------------------------
