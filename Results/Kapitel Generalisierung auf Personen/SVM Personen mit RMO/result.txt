Pre-processing:

Outlier Removal: True
Discrete Wavelet: False
Normalization: False

SVM measures:

	C: 10
	kernel: poly
	degree: 3
	gamma: 0.1
	verbose: True

--------------- Fold 1 ---------------

  Accuracy:	78.19%
  Precision:	79.37%
  Recall:	76.19%
  F1:		77.75%

  TN: 40.09%
  FN: 11.91%
  TP: 38.10%
  FP: 9.90%
---------------------------------------

--------------- Fold 2 ---------------

  Accuracy:	76.87%
  Precision:	77.90%
  Recall:	75.04%
  F1:		76.44%

  TN: 39.35%
  FN: 12.48%
  TP: 37.52%
  FP: 10.65%
---------------------------------------

--------------- Fold 3 ---------------

  Accuracy:	77.44%
  Precision:	78.10%
  Recall:	76.30%
  F1:		77.19%

  TN: 39.28%
  FN: 11.85%
  TP: 38.17%
  FP: 10.70%
---------------------------------------

--------------- Fold 4 ---------------

  Accuracy:	85.50%
  Precision:	82.09%
  Recall:	90.83%
  F1:		86.24%

  TN: 40.06%
  FN: 4.59%
  TP: 45.44%
  FP: 9.91%
---------------------------------------

--------------- Fold 5 ---------------

  Accuracy:	85.40%
  Precision:	81.11%
  Recall:	92.31%
  F1:		86.35%

  TN: 39.21%
  FN: 3.85%
  TP: 46.19%
  FP: 10.76%
---------------------------------------
