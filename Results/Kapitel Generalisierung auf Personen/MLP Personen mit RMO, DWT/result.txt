Pre-processing:

Outlier Removal: True
Discrete Wavelet: True

MLP measures:

	hidden_layer_sizes: (1000, 500)
	max_iter: 16
	activation: relu
	solver: adam
	warm_start: False
	early_stopping: True
	verbose: True

--------------- Fold 1 ---------------

  Accuracy:	94.27%
  Precision:	94.42%
  Recall:	94.10%
  F1:		94.26%

  TN: 47.21%
  FN: 2.95%
  TP: 47.06%
  FP: 2.78%
---------------------------------------

--------------- Fold 2 ---------------

  Accuracy:	94.34%
  Precision:	95.51%
  Recall:	93.05%
  F1:		94.26%

  TN: 47.81%
  FN: 3.47%
  TP: 46.53%
  FP: 2.19%
---------------------------------------

--------------- Fold 3 ---------------

  Accuracy:	92.87%
  Precision:	95.99%
  Recall:	89.49%
  F1:		92.62%

  TN: 48.11%
  FN: 5.26%
  TP: 44.76%
  FP: 1.87%
---------------------------------------

--------------- Fold 4 ---------------

  Accuracy:	95.74%
  Precision:	92.83%
  Recall:	99.16%
  F1:		95.89%

  TN: 46.14%
  FN: 0.42%
  TP: 49.61%
  FP: 3.83%
---------------------------------------

--------------- Fold 5 ---------------

  Accuracy:	95.48%
  Precision:	93.38%
  Recall:	97.92%
  F1:		95.59%

  TN: 46.50%
  FN: 1.04%
  TP: 48.99%
  FP: 3.47%
---------------------------------------
