Pre-processing:

Outlier Removal: False
Discrete Wavelet: False

MLP measures:

	hidden_layer_sizes: (1000, 500)
	max_iter: 16
	activation: relu
	solver: adam
	warm_start: False
	early_stopping: True
	verbose: True

--------------- Fold 1 ---------------

  Accuracy:	90.49%
  Precision:	95.58%
  Recall:	84.91%
  F1:		89.93%

  TN: 48.03%
  FN: 7.55%
  TP: 42.46%
  FP: 1.96%
---------------------------------------

--------------- Fold 2 ---------------

  Accuracy:	90.61%
  Precision:	95.50%
  Recall:	85.25%
  F1:		90.08%

  TN: 47.99%
  FN: 7.38%
  TP: 42.63%
  FP: 2.01%
---------------------------------------

--------------- Fold 3 ---------------

  Accuracy:	89.56%
  Precision:	96.12%
  Recall:	82.46%
  F1:		88.76%

  TN: 48.31%
  FN: 8.77%
  TP: 41.24%
  FP: 1.67%
---------------------------------------

--------------- Fold 4 ---------------

  Accuracy:	97.23%
  Precision:	95.19%
  Recall:	99.50%
  F1:		97.30%

  TN: 47.45%
  FN: 0.25%
  TP: 49.78%
  FP: 2.52%
---------------------------------------

--------------- Fold 5 ---------------

  Accuracy:	96.03%
  Precision:	92.79%
  Recall:	99.83%
  F1:		96.18%

  TN: 46.09%
  FN: 0.09%
  TP: 49.94%
  FP: 3.88%
---------------------------------------
