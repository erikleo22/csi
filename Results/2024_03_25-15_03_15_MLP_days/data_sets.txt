

----------------------------- Split for Fold 1 -----------------------------

train/validation-ratio: 70.0% / 30.0%

training measures:
	data sets:
		CSI_February_12_24_15_28_42,	Saeed		--> 100.0% used, 	6.4%
		CSI_February_12_24_15_32_22,	Saeed		--> 100.0% used, 	5.8%
		CSI_February_6_24_12_04_40,	Alejandro	--> 100.0% used, 	6.6%
		CSI_February_6_24_12_13_05,	Daniel		--> 100.0% used, 	5.8%
		CSI_February_6_24_12_23_20,	Ian		--> 100.0% used, 	5.8%
		CSI_January_11_24_10_57_26,	Alejandro	--> 100.0% used, 	7.9%
		CSI_January_11_24_11_13_37,	Daniel		--> 100.0% used, 	6.3%
		CSI_January_19_24_09_19_15,	Ian		--> 100.0% used, 	5.7%
		CSI_February_12_24_16_40_32,	nobody		--> 68.8% used, 	8.8%
		CSI_February_8_24_16_25_37,	nobody		--> 68.8% used, 	13.0%
		CSI_January_11_24_13_52_25,	nobody		--> 68.8% used, 	8.5%
		CSI_January_16_24_14_05_50,	nobody		--> 68.8% used, 	4.3%
		CSI_January_19_24_08_39_57,	nobody		--> 68.8% used, 	4.2%
		CSI_January_31_24_15_28_25,	nobody		--> 68.8% used, 	11.6%

	data distribution:
		persons:
			Saeed:		12.3%
			Alejandro:	14.5%
			Daniel:		12.0%
			Ian:		11.5%
			nobody:		50.3%

		days:
			February_12:	21.1%
			February_6:	18.2%
			January_11:	22.7%
			January_19:	9.9%
			February_8:	13.0%
			January_16:	4.3%
			January_31:	11.6%

validation measures:
	data sets:
		CSI_March_11_24_13_42_52,	Ana		--> 94.4% used, 	12.6%
		CSI_March_18_24_15_17_18,	Leo		--> 94.4% used, 	11.2%
		CSI_March_18_24_15_20_30,	Ana		--> 94.4% used, 	14.5%
		CSI_March_6_24_10_16_19,	Leo		--> 94.4% used, 	12.2%
		CSI_March_11_24_13_56_31,	nobody		--> 54.7% used, 	17.4%
		CSI_March_18_24_15_23_38,	nobody		--> 54.7% used, 	17.6%
		CSI_March_18_24_15_33_27,	nobody		--> 54.7% used, 	7.6%
		CSI_March_6_24_10_13_18,	nobody		--> 54.7% used, 	7.8%

	data distribution:
		persons:
			Ana:		27.1%
			Leo:		23.4%
			nobody:		50.4%

		days:
			March_11:	30.0%
			March_18:	50.9%
			March_6:	20.0%


----------------------------- Split for Fold 2 -----------------------------

train/validation-ratio: 70.0% / 30.0%

training measures:
	data sets:
		CSI_February_12_24_15_28_42,	Saeed		--> 100.0% used, 	6.2%
		CSI_February_12_24_15_32_22,	Saeed		--> 100.0% used, 	5.6%
		CSI_February_6_24_12_04_40,	Alejandro	--> 100.0% used, 	6.3%
		CSI_February_6_24_12_13_05,	Daniel		--> 100.0% used, 	5.5%
		CSI_February_6_24_12_28_39,	Florian		--> 100.0% used, 	6.8%
		CSI_January_11_24_10_57_26,	Alejandro	--> 100.0% used, 	7.6%
		CSI_January_11_24_11_09_46,	Florian		--> 100.0% used, 	6.1%
		CSI_January_11_24_11_13_37,	Daniel		--> 100.0% used, 	6.0%
		CSI_February_12_24_16_40_32,	nobody		--> 71.4% used, 	8.8%
		CSI_February_8_24_16_25_37,	nobody		--> 71.4% used, 	13.0%
		CSI_January_11_24_13_52_25,	nobody		--> 71.4% used, 	8.5%
		CSI_January_16_24_14_05_50,	nobody		--> 71.4% used, 	4.3%
		CSI_January_19_24_08_39_57,	nobody		--> 71.4% used, 	4.2%
		CSI_January_31_24_15_28_25,	nobody		--> 71.4% used, 	11.5%

	data distribution:
		persons:
			Saeed:		11.8%
			Alejandro:	14.0%
			Daniel:		11.6%
			Florian:	12.9%
			nobody:		50.3%

		days:
			February_12:	20.7%
			February_6:	18.7%
			January_11:	28.3%
			February_8:	13.0%
			January_16:	4.3%
			January_19:	4.2%
			January_31:	11.5%

validation measures:
	data sets:
		CSI_March_11_24_13_42_52,	Ana		--> 98.0% used, 	12.5%
		CSI_March_18_24_15_17_18,	Leo		--> 98.0% used, 	11.2%
		CSI_March_18_24_15_20_30,	Ana		--> 98.0% used, 	14.5%
		CSI_March_6_24_10_16_19,	Leo		--> 98.0% used, 	12.2%
		CSI_March_11_24_13_56_31,	nobody		--> 56.7% used, 	17.4%
		CSI_March_18_24_15_23_38,	nobody		--> 56.7% used, 	17.6%
		CSI_March_18_24_15_33_27,	nobody		--> 56.7% used, 	7.6%
		CSI_March_6_24_10_13_18,	nobody		--> 56.7% used, 	7.8%

	data distribution:
		persons:
			Ana:		27.1%
			Leo:		23.3%
			nobody:		50.4%

		days:
			March_11:	30.0%
			March_18:	50.9%
			March_6:	20.0%


----------------------------- Split for Fold 3 -----------------------------

train/validation-ratio: 70.0% / 30.0%

training measures:
	data sets:
		CSI_February_12_24_15_28_42,	Saeed		--> 100.0% used, 	6.2%
		CSI_February_12_24_15_32_22,	Saeed		--> 100.0% used, 	5.7%
		CSI_February_6_24_12_04_40,	Alejandro	--> 100.0% used, 	6.4%
		CSI_February_6_24_12_23_20,	Ian		--> 100.0% used, 	5.7%
		CSI_February_6_24_12_28_39,	Florian		--> 100.0% used, 	6.9%
		CSI_January_11_24_10_57_26,	Alejandro	--> 100.0% used, 	7.7%
		CSI_January_11_24_11_09_46,	Florian		--> 100.0% used, 	6.2%
		CSI_January_19_24_09_19_15,	Ian		--> 100.0% used, 	5.6%
		CSI_February_12_24_16_40_32,	nobody		--> 70.7% used, 	8.8%
		CSI_February_8_24_16_25_37,	nobody		--> 70.7% used, 	13.0%
		CSI_January_11_24_13_52_25,	nobody		--> 70.7% used, 	8.5%
		CSI_January_16_24_14_05_50,	nobody		--> 70.7% used, 	4.3%
		CSI_January_19_24_08_39_57,	nobody		--> 70.7% used, 	4.2%
		CSI_January_31_24_15_28_25,	nobody		--> 70.7% used, 	11.6%

	data distribution:
		persons:
			Saeed:		11.9%
			Alejandro:	14.1%
			Ian:		11.2%
			Florian:	13.1%
			nobody:		50.3%

		days:
			February_12:	20.8%
			February_6:	19.0%
			January_11:	22.4%
			January_19:	9.7%
			February_8:	13.0%
			January_16:	4.3%
			January_31:	11.6%

validation measures:
	data sets:
		CSI_March_11_24_13_42_52,	Ana		--> 97.1% used, 	12.5%
		CSI_March_18_24_15_17_18,	Leo		--> 97.1% used, 	11.2%
		CSI_March_18_24_15_20_30,	Ana		--> 97.1% used, 	14.5%
		CSI_March_6_24_10_16_19,	Leo		--> 97.1% used, 	12.2%
		CSI_March_11_24_13_56_31,	nobody		--> 56.2% used, 	17.4%
		CSI_March_18_24_15_23_38,	nobody		--> 56.2% used, 	17.6%
		CSI_March_18_24_15_33_27,	nobody		--> 56.2% used, 	7.6%
		CSI_March_6_24_10_13_18,	nobody		--> 56.2% used, 	7.8%

	data distribution:
		persons:
			Ana:		27.1%
			Leo:		23.4%
			nobody:		50.4%

		days:
			March_11:	30.0%
			March_18:	50.9%
			March_6:	20.0%


----------------------------- Split for Fold 4 -----------------------------

train/validation-ratio: 70.0% / 30.0%

training measures:
	data sets:
		CSI_February_12_24_15_28_42,	Saeed		--> 100.0% used, 	6.6%
		CSI_February_12_24_15_32_22,	Saeed		--> 100.0% used, 	6.0%
		CSI_February_6_24_12_13_05,	Daniel		--> 100.0% used, 	5.9%
		CSI_February_6_24_12_23_20,	Ian		--> 100.0% used, 	5.9%
		CSI_February_6_24_12_28_39,	Florian		--> 100.0% used, 	7.3%
		CSI_January_11_24_11_09_46,	Florian		--> 100.0% used, 	6.5%
		CSI_January_11_24_11_13_37,	Daniel		--> 100.0% used, 	6.4%
		CSI_January_19_24_09_19_15,	Ian		--> 100.0% used, 	5.8%
		CSI_February_12_24_16_40_32,	nobody		--> 67.3% used, 	8.9%
		CSI_February_8_24_16_25_37,	nobody		--> 67.3% used, 	13.0%
		CSI_January_11_24_13_52_25,	nobody		--> 67.3% used, 	8.5%
		CSI_January_16_24_14_05_50,	nobody		--> 67.3% used, 	4.3%
		CSI_January_19_24_08_39_57,	nobody		--> 67.3% used, 	4.2%
		CSI_January_31_24_15_28_25,	nobody		--> 67.3% used, 	11.6%

	data distribution:
		persons:
			Saeed:		12.5%
			Daniel:		12.3%
			Ian:		11.8%
			Florian:	13.7%
			nobody:		50.3%

		days:
			February_12:	21.4%
			February_6:	19.1%
			January_11:	21.4%
			January_19:	10.0%
			February_8:	13.0%
			January_16:	4.3%
			January_31:	11.6%

validation measures:
	data sets:
		CSI_March_11_24_13_42_52,	Ana		--> 92.4% used, 	12.5%
		CSI_March_18_24_15_17_18,	Leo		--> 92.4% used, 	11.2%
		CSI_March_18_24_15_20_30,	Ana		--> 92.4% used, 	14.5%
		CSI_March_6_24_10_16_19,	Leo		--> 92.4% used, 	12.2%
		CSI_March_11_24_13_56_31,	nobody		--> 53.5% used, 	17.4%
		CSI_March_18_24_15_23_38,	nobody		--> 53.5% used, 	17.6%
		CSI_March_18_24_15_33_27,	nobody		--> 53.5% used, 	7.5%
		CSI_March_6_24_10_13_18,	nobody		--> 53.5% used, 	7.8%

	data distribution:
		persons:
			Ana:		27.1%
			Leo:		23.4%
			nobody:		50.4%

		days:
			March_11:	30.0%
			March_18:	50.9%
			March_6:	20.0%


----------------------------- Split for Fold 5 -----------------------------

train/validation-ratio: 70.0% / 30.0%

training measures:
	data sets:
		CSI_February_6_24_12_04_40,	Alejandro	--> 100.0% used, 	6.4%
		CSI_February_6_24_12_13_05,	Daniel		--> 100.0% used, 	5.6%
		CSI_February_6_24_12_23_20,	Ian		--> 100.0% used, 	5.7%
		CSI_February_6_24_12_28_39,	Florian		--> 100.0% used, 	6.9%
		CSI_January_11_24_10_57_26,	Alejandro	--> 100.0% used, 	7.7%
		CSI_January_11_24_11_09_46,	Florian		--> 100.0% used, 	6.2%
		CSI_January_11_24_11_13_37,	Daniel		--> 100.0% used, 	6.1%
		CSI_January_19_24_09_19_15,	Ian		--> 100.0% used, 	5.6%
		CSI_February_12_24_16_40_32,	nobody		--> 70.4% used, 	8.8%
		CSI_February_8_24_16_25_37,	nobody		--> 70.4% used, 	13.0%
		CSI_January_11_24_13_52_25,	nobody		--> 70.4% used, 	8.5%
		CSI_January_16_24_14_05_50,	nobody		--> 70.4% used, 	4.3%
		CSI_January_19_24_08_39_57,	nobody		--> 70.4% used, 	4.2%
		CSI_January_31_24_15_28_25,	nobody		--> 70.4% used, 	11.5%

	data distribution:
		persons:
			Alejandro:	14.2%
			Daniel:		11.7%
			Ian:		11.3%
			Florian:	13.1%
			nobody:		50.3%

		days:
			February_6:	24.7%
			January_11:	28.5%
			January_19:	9.8%
			February_12:	8.8%
			February_8:	13.0%
			January_16:	4.3%
			January_31:	11.5%

validation measures:
	data sets:
		CSI_March_11_24_13_42_52,	Ana		--> 96.6% used, 	12.5%
		CSI_March_18_24_15_17_18,	Leo		--> 96.6% used, 	11.2%
		CSI_March_18_24_15_20_30,	Ana		--> 96.6% used, 	14.5%
		CSI_March_6_24_10_16_19,	Leo		--> 96.6% used, 	12.2%
		CSI_March_11_24_13_56_31,	nobody		--> 55.9% used, 	17.4%
		CSI_March_18_24_15_23_38,	nobody		--> 55.9% used, 	17.6%
		CSI_March_18_24_15_33_27,	nobody		--> 55.9% used, 	7.6%
		CSI_March_6_24_10_13_18,	nobody		--> 55.9% used, 	7.8%

	data distribution:
		persons:
			Ana:		27.1%
			Leo:		23.3%
			nobody:		50.4%

		days:
			March_11:	30.0%
			March_18:	50.9%
			March_6:	20.0%
