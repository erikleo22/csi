MLP measures:

	hidden_layer_sizes: (500, 100)
	max_iter: 10
	activation: relu
	solver: adam
	warm_start: False
	early_stopping: True
	verbose: True

--------------- Fold 1 ---------------

  Accuracy:	49.86%
  Precision:	49.94%
  Recall:	99.60%
  F1:		66.52%

  TN: 0.05%
  FN: 0.20%
  TP: 49.81%
  FP: 49.94%
---------------------------------------

--------------- Fold 2 ---------------

  Accuracy:	49.97%
  Precision:	49.99%
  Recall:	99.82%
  F1:		66.62%

  TN: 0.05%
  FN: 0.09%
  TP: 49.92%
  FP: 49.94%
---------------------------------------

--------------- Fold 3 ---------------

  Accuracy:	49.52%
  Precision:	49.76%
  Recall:	98.49%
  F1:		66.12%

  TN: 0.26%
  FN: 0.75%
  TP: 49.26%
  FP: 49.73%
---------------------------------------

--------------- Fold 4 ---------------

  Accuracy:	49.99%
  Precision:	50.00%
  Recall:	99.96%
  F1:		66.66%

  TN: 0.00%
  FN: 0.02%
  TP: 49.99%
  FP: 49.99%
---------------------------------------

--------------- Fold 5 ---------------

  Accuracy:	49.71%
  Precision:	49.86%
  Recall:	99.40%
  F1:		66.41%

  TN: 0.00%
  FN: 0.30%
  TP: 49.71%
  FP: 49.99%
---------------------------------------
