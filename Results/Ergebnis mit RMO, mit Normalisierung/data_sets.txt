

----------------------------- Split for Fold 1 -----------------------------

train/validation-ratio: 70.0% / 30.0%

training measures:
	data sets:
		CSI_February_12_24_15_28_42,	Saeed		--> 100.0% used, 	6.4%
		CSI_February_12_24_15_32_22,	Saeed		--> 100.0% used, 	5.8%
		CSI_February_6_24_12_04_40,	Alejandro	--> 100.0% used, 	6.5%
		CSI_February_6_24_12_13_05,	Daniel		--> 100.0% used, 	5.7%
		CSI_February_6_24_12_23_20,	Ian		--> 100.0% used, 	5.8%
		CSI_January_11_24_10_57_26,	Alejandro	--> 100.0% used, 	7.9%
		CSI_January_11_24_11_13_37,	Daniel		--> 100.0% used, 	6.2%
		CSI_January_19_24_09_19_15,	Ian		--> 100.0% used, 	5.7%
		CSI_February_12_24_16_40_32,	nobody		--> 68.8% used, 	8.8%
		CSI_February_8_24_16_25_37,	nobody		--> 68.8% used, 	12.9%
		CSI_January_11_24_13_52_25,	nobody		--> 68.8% used, 	8.4%
		CSI_January_16_24_14_05_50,	nobody		--> 68.8% used, 	4.3%
		CSI_January_19_24_08_39_57,	nobody		--> 68.8% used, 	4.2%
		CSI_January_31_24_15_28_25,	nobody		--> 68.8% used, 	11.5%

	data distribution:
		persons:
			Saeed:		12.2%
			Alejandro:	14.4%
			Daniel:		11.9%
			Ian:		11.5%
			nobody:		50.0%

		days:
			February_12:	21.0%
			February_6:	18.0%
			January_11:	22.5%
			January_19:	9.8%
			February_8:	12.9%
			January_16:	4.3%
			January_31:	11.5%

validation measures:
	data sets:
		CSI_January_16_24_14_31_55,	Leo		--> 93.8% used, 	12.3%
		CSI_March_11_24_13_42_52,	Ana		--> 93.8% used, 	12.4%
		CSI_March_18_24_15_17_18,	Leo		--> 93.8% used, 	11.0%
		CSI_March_18_24_15_20_30,	Ana		--> 93.8% used, 	14.3%
		CSI_January_16_24_14_05_50,	nobody		--> 54.5% used, 	7.9%
		CSI_March_11_24_13_56_31,	nobody		--> 54.5% used, 	17.2%
		CSI_March_18_24_15_23_38,	nobody		--> 54.5% used, 	17.4%
		CSI_March_18_24_15_33_27,	nobody		--> 54.5% used, 	7.5%

	data distribution:
		persons:
			Leo:		23.3%
			Ana:		26.7%
			nobody:		50.0%

		days:
			January_16:	20.2%
			March_11:	29.6%
			March_18:	50.2%


----------------------------- Split for Fold 2 -----------------------------

train/validation-ratio: 70.0% / 30.0%

training measures:
	data sets:
		CSI_February_12_24_15_28_42,	Saeed		--> 100.0% used, 	6.2%
		CSI_February_12_24_15_32_22,	Saeed		--> 100.0% used, 	5.6%
		CSI_February_6_24_12_04_40,	Alejandro	--> 100.0% used, 	6.3%
		CSI_February_6_24_12_13_05,	Daniel		--> 100.0% used, 	5.5%
		CSI_February_6_24_12_28_39,	Florian		--> 100.0% used, 	6.8%
		CSI_January_11_24_10_57_26,	Alejandro	--> 100.0% used, 	7.6%
		CSI_January_11_24_11_09_46,	Florian		--> 100.0% used, 	6.1%
		CSI_January_11_24_11_13_37,	Daniel		--> 100.0% used, 	6.0%
		CSI_February_12_24_16_40_32,	nobody		--> 71.4% used, 	8.8%
		CSI_February_8_24_16_25_37,	nobody		--> 71.4% used, 	12.9%
		CSI_January_11_24_13_52_25,	nobody		--> 71.4% used, 	8.4%
		CSI_January_16_24_14_05_50,	nobody		--> 71.4% used, 	4.3%
		CSI_January_19_24_08_39_57,	nobody		--> 71.4% used, 	4.1%
		CSI_January_31_24_15_28_25,	nobody		--> 71.4% used, 	11.5%

	data distribution:
		persons:
			Saeed:		11.7%
			Alejandro:	13.9%
			Daniel:		11.5%
			Florian:	12.9%
			nobody:		50.0%

		days:
			February_12:	20.5%
			February_6:	18.6%
			January_11:	28.1%
			February_8:	12.9%
			January_16:	4.3%
			January_19:	4.1%
			January_31:	11.5%

validation measures:
	data sets:
		CSI_January_16_24_14_31_55,	Leo		--> 97.4% used, 	12.3%
		CSI_March_11_24_13_42_52,	Ana		--> 97.4% used, 	12.4%
		CSI_March_18_24_15_17_18,	Leo		--> 97.4% used, 	11.0%
		CSI_March_18_24_15_20_30,	Ana		--> 97.4% used, 	14.3%
		CSI_January_16_24_14_05_50,	nobody		--> 56.6% used, 	7.9%
		CSI_March_11_24_13_56_31,	nobody		--> 56.6% used, 	17.2%
		CSI_March_18_24_15_23_38,	nobody		--> 56.6% used, 	17.4%
		CSI_March_18_24_15_33_27,	nobody		--> 56.6% used, 	7.5%

	data distribution:
		persons:
			Leo:		23.3%
			Ana:		26.7%
			nobody:		50.0%

		days:
			January_16:	20.2%
			March_11:	29.6%
			March_18:	50.2%


----------------------------- Split for Fold 3 -----------------------------

train/validation-ratio: 70.0% / 30.0%

training measures:
	data sets:
		CSI_February_12_24_15_28_42,	Saeed		--> 100.0% used, 	6.2%
		CSI_February_12_24_15_32_22,	Saeed		--> 100.0% used, 	5.6%
		CSI_February_6_24_12_04_40,	Alejandro	--> 100.0% used, 	6.4%
		CSI_February_6_24_12_23_20,	Ian		--> 100.0% used, 	5.6%
		CSI_February_6_24_12_28_39,	Florian		--> 100.0% used, 	6.9%
		CSI_January_11_24_10_57_26,	Alejandro	--> 100.0% used, 	7.7%
		CSI_January_11_24_11_09_46,	Florian		--> 100.0% used, 	6.1%
		CSI_January_19_24_09_19_15,	Ian		--> 100.0% used, 	5.5%
		CSI_February_12_24_16_40_32,	nobody		--> 70.7% used, 	8.8%
		CSI_February_8_24_16_25_37,	nobody		--> 70.7% used, 	12.9%
		CSI_January_11_24_13_52_25,	nobody		--> 70.7% used, 	8.4%
		CSI_January_16_24_14_05_50,	nobody		--> 70.7% used, 	4.3%
		CSI_January_19_24_08_39_57,	nobody		--> 70.7% used, 	4.1%
		CSI_January_31_24_15_28_25,	nobody		--> 70.7% used, 	11.5%

	data distribution:
		persons:
			Saeed:		11.9%
			Alejandro:	14.0%
			Ian:		11.1%
			Florian:	13.0%
			nobody:		50.0%

		days:
			February_12:	20.6%
			February_6:	18.8%
			January_11:	22.2%
			January_19:	9.7%
			February_8:	12.9%
			January_16:	4.3%
			January_31:	11.5%

validation measures:
	data sets:
		CSI_January_16_24_14_31_55,	Leo		--> 96.5% used, 	12.3%
		CSI_March_11_24_13_42_52,	Ana		--> 96.5% used, 	12.4%
		CSI_March_18_24_15_17_18,	Leo		--> 96.5% used, 	11.0%
		CSI_March_18_24_15_20_30,	Ana		--> 96.5% used, 	14.3%
		CSI_January_16_24_14_05_50,	nobody		--> 56.1% used, 	7.9%
		CSI_March_11_24_13_56_31,	nobody		--> 56.1% used, 	17.2%
		CSI_March_18_24_15_23_38,	nobody		--> 56.1% used, 	17.4%
		CSI_March_18_24_15_33_27,	nobody		--> 56.1% used, 	7.5%

	data distribution:
		persons:
			Leo:		23.3%
			Ana:		26.7%
			nobody:		50.0%

		days:
			January_16:	20.2%
			March_11:	29.6%
			March_18:	50.3%


----------------------------- Split for Fold 4 -----------------------------

train/validation-ratio: 70.0% / 30.0%

training measures:
	data sets:
		CSI_February_12_24_15_28_42,	Saeed		--> 100.0% used, 	6.5%
		CSI_February_12_24_15_32_22,	Saeed		--> 100.0% used, 	5.9%
		CSI_February_6_24_12_13_05,	Daniel		--> 100.0% used, 	5.8%
		CSI_February_6_24_12_23_20,	Ian		--> 100.0% used, 	5.9%
		CSI_February_6_24_12_28_39,	Florian		--> 100.0% used, 	7.2%
		CSI_January_11_24_11_09_46,	Florian		--> 100.0% used, 	6.4%
		CSI_January_11_24_11_13_37,	Daniel		--> 100.0% used, 	6.4%
		CSI_January_19_24_09_19_15,	Ian		--> 100.0% used, 	5.8%
		CSI_February_12_24_16_40_32,	nobody		--> 67.3% used, 	8.8%
		CSI_February_8_24_16_25_37,	nobody		--> 67.3% used, 	12.9%
		CSI_January_11_24_13_52_25,	nobody		--> 67.3% used, 	8.4%
		CSI_January_16_24_14_05_50,	nobody		--> 67.3% used, 	4.3%
		CSI_January_19_24_08_39_57,	nobody		--> 67.3% used, 	4.1%
		CSI_January_31_24_15_28_25,	nobody		--> 67.3% used, 	11.5%

	data distribution:
		persons:
			Saeed:		12.5%
			Daniel:		12.2%
			Ian:		11.7%
			Florian:	13.7%
			nobody:		50.0%

		days:
			February_12:	21.3%
			February_6:	19.0%
			January_11:	21.2%
			January_19:	9.9%
			February_8:	12.9%
			January_16:	4.3%
			January_31:	11.5%

validation measures:
	data sets:
		CSI_January_16_24_14_31_55,	Leo		--> 91.8% used, 	12.3%
		CSI_March_11_24_13_42_52,	Ana		--> 91.8% used, 	12.4%
		CSI_March_18_24_15_17_18,	Leo		--> 91.8% used, 	11.0%
		CSI_March_18_24_15_20_30,	Ana		--> 91.8% used, 	14.3%
		CSI_January_16_24_14_05_50,	nobody		--> 53.4% used, 	7.9%
		CSI_March_11_24_13_56_31,	nobody		--> 53.4% used, 	17.2%
		CSI_March_18_24_15_23_38,	nobody		--> 53.4% used, 	17.4%
		CSI_March_18_24_15_33_27,	nobody		--> 53.4% used, 	7.5%

	data distribution:
		persons:
			Leo:		23.3%
			Ana:		26.7%
			nobody:		50.0%

		days:
			January_16:	20.2%
			March_11:	29.6%
			March_18:	50.3%


----------------------------- Split for Fold 5 -----------------------------

train/validation-ratio: 70.0% / 30.0%

training measures:
	data sets:
		CSI_February_6_24_12_04_40,	Alejandro	--> 100.0% used, 	6.4%
		CSI_February_6_24_12_13_05,	Daniel		--> 100.0% used, 	5.6%
		CSI_February_6_24_12_23_20,	Ian		--> 100.0% used, 	5.6%
		CSI_February_6_24_12_28_39,	Florian		--> 100.0% used, 	6.9%
		CSI_January_11_24_10_57_26,	Alejandro	--> 100.0% used, 	7.7%
		CSI_January_11_24_11_09_46,	Florian		--> 100.0% used, 	6.2%
		CSI_January_11_24_11_13_37,	Daniel		--> 100.0% used, 	6.1%
		CSI_January_19_24_09_19_15,	Ian		--> 100.0% used, 	5.5%
		CSI_February_12_24_16_40_32,	nobody		--> 70.4% used, 	8.8%
		CSI_February_8_24_16_25_37,	nobody		--> 70.4% used, 	12.9%
		CSI_January_11_24_13_52_25,	nobody		--> 70.4% used, 	8.4%
		CSI_January_16_24_14_05_50,	nobody		--> 70.4% used, 	4.3%
		CSI_January_19_24_08_39_57,	nobody		--> 70.4% used, 	4.1%
		CSI_January_31_24_15_28_25,	nobody		--> 70.4% used, 	11.5%

	data distribution:
		persons:
			Alejandro:	14.1%
			Daniel:		11.7%
			Ian:		11.2%
			Florian:	13.1%
			nobody:		50.0%

		days:
			February_6:	24.5%
			January_11:	28.4%
			January_19:	9.7%
			February_12:	8.8%
			February_8:	12.9%
			January_16:	4.3%
			January_31:	11.5%

validation measures:
	data sets:
		CSI_January_16_24_14_31_55,	Leo		--> 96.0% used, 	12.3%
		CSI_March_11_24_13_42_52,	Ana		--> 96.0% used, 	12.4%
		CSI_March_18_24_15_17_18,	Leo		--> 96.0% used, 	11.0%
		CSI_March_18_24_15_20_30,	Ana		--> 96.0% used, 	14.3%
		CSI_January_16_24_14_05_50,	nobody		--> 55.8% used, 	7.9%
		CSI_March_11_24_13_56_31,	nobody		--> 55.8% used, 	17.2%
		CSI_March_18_24_15_23_38,	nobody		--> 55.8% used, 	17.4%
		CSI_March_18_24_15_33_27,	nobody		--> 55.8% used, 	7.5%

	data distribution:
		persons:
			Leo:		23.3%
			Ana:		26.7%
			nobody:		50.0%

		days:
			January_16:	20.2%
			March_11:	29.6%
			March_18:	50.2%
