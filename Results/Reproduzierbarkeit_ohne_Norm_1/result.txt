Pre-processing:

Outlier Removal: True
Discrete Wavelet: False

MLP measures:

	hidden_layer_sizes: (1000, 500)
	max_iter: 8
	activation: relu
	solver: adam
	warm_start: False
	early_stopping: True
	verbose: True

--------------- Fold 1 ---------------

  Accuracy:	69.58%
  Precision:	67.20%
  Recall:	76.50%
  F1:		71.55%

  TN: 31.33%
  FN: 11.75%
  TP: 38.25%
  FP: 18.67%
---------------------------------------

--------------- Fold 2 ---------------

  Accuracy:	52.05%
  Precision:	51.21%
  Recall:	86.64%
  F1:		64.37%

  TN: 8.73%
  FN: 6.68%
  TP: 43.32%
  FP: 41.27%
---------------------------------------

--------------- Fold 3 ---------------

  Accuracy:	68.20%
  Precision:	63.17%
  Recall:	87.27%
  F1:		73.29%

  TN: 24.57%
  FN: 6.37%
  TP: 43.63%
  FP: 25.44%
---------------------------------------

--------------- Fold 4 ---------------

  Accuracy:	54.91%
  Precision:	53.17%
  Recall:	82.41%
  F1:		64.63%

  TN: 13.70%
  FN: 8.80%
  TP: 41.20%
  FP: 36.30%
---------------------------------------

--------------- Fold 5 ---------------

  Accuracy:	61.47%
  Precision:	62.47%
  Recall:	57.45%
  F1:		59.86%

  TN: 32.75%
  FN: 21.27%
  TP: 28.72%
  FP: 17.25%
---------------------------------------
