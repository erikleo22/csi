Pre-processing:

Outlier Removal: False
Discrete Wavelet: False
Normalization: False

MLP measures:

	hidden_layer_sizes: (1000, 500)
	max_iter: 16
	activation: relu
	solver: adam
	warm_start: False
	early_stopping: True
	verbose: True
	batch_size: 32

--------------- Fold 1 ---------------

  Accuracy:	48.94%
  Precision:	49.46%
  Recall:	97.25%
  F1:		65.57%

  TN: 0.31%
  FN: 1.38%
  TP: 48.63%
  FP: 49.68%
---------------------------------------

--------------- Fold 2 ---------------

  Accuracy:	54.14%
  Precision:	52.32%
  Recall:	93.26%
  F1:		67.03%

  TN: 7.50%
  FN: 3.37%
  TP: 46.63%
  FP: 42.50%
---------------------------------------

--------------- Fold 3 ---------------

  Accuracy:	46.69%
  Precision:	48.23%
  Recall:	90.10%
  F1:		62.83%

  TN: 1.64%
  FN: 4.95%
  TP: 45.05%
  FP: 48.36%
---------------------------------------

--------------- Fold 4 ---------------

  Accuracy:	49.47%
  Precision:	49.73%
  Recall:	98.16%
  F1:		66.01%

  TN: 0.39%
  FN: 0.92%
  TP: 49.07%
  FP: 49.61%
---------------------------------------

--------------- Fold 5 ---------------

  Accuracy:	48.45%
  Precision:	47.66%
  Recall:	31.55%
  F1:		37.97%

  TN: 32.68%
  FN: 34.22%
  TP: 15.78%
  FP: 17.32%
---------------------------------------
