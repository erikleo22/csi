import adafruit_dht
import board
import time
from datetime import date, datetime
import csv

sensor = adafruit_dht.DHT22(board.SCL)
filename = 'temperature_and_humidity.csv'

while True:
    with open(filename, 'a+', newline = '') as csvfile:
        try:
            humidity = sensor.humidity
            temperature = sensor.temperature
            
        except Exception as e:
            #print(e)
            continue

        csvwriter = csv.writer(csvfile)
        date_and_time = datetime.now().strftime("%Y-%m-%d_%H:%M:%S")
        print("Wrote to csv file")
        csvwriter.writerow([date_and_time, str(humidity) + '%', str(temperature) + 'degC'])

        time.sleep(10.0)

