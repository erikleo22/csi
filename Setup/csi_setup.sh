#! /bin/bash

#######################################################################
################## preparation of nexmon and nexutil ##################

# navigate to nexmon root directory
cd /home/pi/nexmon

# tutorial pt. 8
source setup_env.sh
make

# tutorial pt. 9
cd /home/pi/nexmon/patches/bcm43455c0/7_45_189/nexmon_csi

# tutorial pt. 10
make install-firmware

# tutorial pt. 11
cd /home/pi/nexmon/utilities/nexutil
make && make install

# tutorial pt. 12
apt-get remove wpasupplicant


#######################################################################
############### set up of csi measurement with nexutil ################

# getting started pt. 1
cd /home/pi/nexmon/patches/bcm43455c0/7_45_189/nexmon_csi/utils/makecsiparams

# getting started pt. 2
pkill wpa_supplicant

# getting started pt. 3
ifconfig wlan0 up

# getting started pt. 4 (5.0 GHz, channel 56, bandwidth 40 MHz) --> more options in config.txt
nexutil -Iwlan0 -s500 -b -l34 -v NtkBEQAAAQAMcnTDFF8AAAAAAAAAAAAAAAAAAAAAAAAAAA==

# getting started pt. 5
iw phy `iw dev wlan0 info | gawk '/wiphy/ {printf "phy" $2}'` interface add mon0 type monitor
ifconfig mon0 up

# getting started pt. 6
tcpdump -i wlan0 dst port 5500