# create mask for nexutil (channel 10, bandwidth 20 MHz, MAC-Address 0C:72:74:C3:14:60)
./makecsiparams -c 10/20 -C 1 -N 1 -m 0C:72:74:C3:14:60

# mask for Fritz!Box (MAC: 0C:72:74:C3:14:60, 10/20, 2.4 GHz):
ChABEQAAAQAMcnTDFGAAAAAAAAAAAAAAAAAAAAAAAAAAAA==

# mask for Fritz!Box (MAC: 0C:72:74:C3:14:5F, 56/80, 5.0 GHz):
OuEBEQAAAQAMcnTDFF8AAAAAAAAAAAAAAAAAAAAAAAAAAA==

# mask for Fritz!Box (MAC: 0C:72:74:C3:14:5F, 56/40, 5.0 GHz):
NtkBEQAAAQAMcnTDFF8AAAAAAAAAAAAAAAAAAAAAAAAAAA==

# mask for Fritz!Box (MAC: 0C:72:74:C3:14:5F, 56/20, 5.0 GHz):
ONABEQAAAQAMcnTDFF8AAAAAAAAAAAAAAAAAAAAAAAAAAA==


# set up wlan0 for nexutil mask
nexutil -Iwlan0 -s500 -b -l34 -v ChABEQAAAQAMcnTDFGAAAAAAAAAAAAAAAAAAAAAAAAAAAA==

# add mon0
iw phy `iw dev wlan0 info | gawk '/wiphy/ {printf "phy" $2}'` interface add mon0 type monitor
